//
// © COPYRIGHT 2023 - Tous Droits Réservés
// ©HANS MATTHIS
//



function goToMiddleOfPage() {
    // Récupère la hauteur de la page
    var pageHeight = document.body.scrollHeight;
  
    // Calcule la position y qui amène l'utilisateur au milieu de la page
    var yPosition = pageHeight / 2.1;
  
    // Déplace le curseur vers la position y
    window.scrollTo(0, yPosition);
  }